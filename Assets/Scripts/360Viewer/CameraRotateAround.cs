﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Image360
{
    public class CameraRotateAround : MonoBehaviour
    {
        #region Unity Editor

        [SerializeField]
        private Transform target;

        [SerializeField]
        private float speedMod = 10;

        [SerializeField]
        private Viewer3D360 _viewer;

        #endregion
        #region Unity 

        void Update()
        {
            if (Input.GetMouseButton(1))
            {
                _viewer.IsFaceToCam = true;

                transform.RotateAround(target.position, Vector3.up * Input.GetAxisRaw("Mouse X"), 20 * Time.deltaTime * speedMod);
                transform.LookAt(target);
            }

            // if (Input.GetMouseButton(0))
            // {
            //     transform.RotateAround(target.position, Vector3.right * Input.GetAxisRaw("Mouse Y"), 20 * Time.deltaTime * speedMod);

            //     transform.LookAt(target);
            // }
        }

        #endregion
    }
}
