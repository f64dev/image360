﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Image360
{
    public class Viewer360 : MonoBehaviour
    {
        #region Unity Editor

        [SerializeField]
        private float _position;

        [SerializeField]
        private string _resourcesFolder = "Eldorado";

        [SerializeField]
        private string _nameModel = "71164148";

        [SerializeField]
        private SpriteRenderer _spriteRenderer;

        #endregion
        #region Unity

        void Start()
        {
            GetSprites(_nameModel);
            Init();
        }



        #endregion
        #region Sprites

        private Sprite[] _sprites;

        public void GetSprites(string name)
        {
            _nameModel = name;

            var array = Resources.LoadAll<Sprite>(_resourcesFolder + "/" + _nameModel);

            _sprites = array.OrderBy(i => int.Parse(i.name)).ToArray();
        }

        public void SetSprite(Sprite sprite)
        {
            _spriteRenderer.sprite = sprite;
        }


        public void SetPosition(float position)
        {
            _position = position;

            var count = _sprites.Length - 1;
            var idx = Mathf.RoundToInt(count * _position);

            SetSprite(_sprites[idx]);
        }

        #endregion
        #region Virtual

        protected virtual void Init()
        {

        }

        #endregion
    }
}
