﻿using UnityEngine;

namespace Image360
{
    public class Viewer3D360 : Viewer360
    {
        #region Unity Editor

        [SerializeField]
        protected Camera _camera;

        #endregion
        #region Unity

        public bool IsFaceToCam { get; set; }

        void LateUpdate()
        {
            transform.LookAt(_camera.transform.position);

            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

            if (IsFaceToCam)
                SetPosition(GetPosition(_camera.transform.position, transform.position));
        }

        #endregion
        #region Position

        public static float GetPosition(Vector3 p1, Vector3 p2)
        {
            return (Mathf.Atan2(p2.z - p1.z, p2.x - p1.x) * 180 / Mathf.PI + 180) / 360;
        }


        #endregion
        #region  Override

        protected override void Init()
        {
            IsFaceToCam = true;
        }

        #endregion
    }
}
