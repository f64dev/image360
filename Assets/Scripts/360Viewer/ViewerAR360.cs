﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Image360
{
    public class ViewerAR360 : Viewer3D360
    {
        #region Unity

        void LateUpdate()
        {
            transform.LookAt(_camera.transform.position);

            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

            if (IsFaceToCam)
                SetPosition(1 - GetPosition(_camera.transform.position, transform.position));
        }

        #endregion
    }
}
