﻿using System.Collections;
using System.Collections.Generic;
using Image360;
using UnityEngine;

public class ViewerPicker : MonoBehaviour
{
    #region Unity Editor

    [SerializeField]
    private Camera _camera;

    [SerializeField]
    private Transform _obj;

    [SerializeField]
    private float _offset = 6;

    [SerializeField]
    private Viewer3D360 _viewer;

    #endregion
    #region Unity

    private void Update()
    {
        MoveUpdate();
    }

    #endregion
    #region Move

    private bool _isPicked;

    public void MoveUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var ray = _camera.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000) && hit.transform == _obj)
                _isPicked = true;
        }


        if (_isPicked)
        {
            _viewer.IsFaceToCam = false;

            var ray = _camera.ScreenPointToRay(Input.mousePosition);

            _obj.position = ray.origin + ray.direction * _offset;
        }

        if (Input.GetMouseButtonUp(0))
            _isPicked = false;

    }

    #endregion
}
