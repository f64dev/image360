﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Net;
using System.IO;
using Image360.Utility;

namespace Image360
{
    public class EldoradoDownloader : EditorWindow
    {
        #region Init

        private string _baseUrl = "https://static.eldorado.ru/upload/3d/";
        private string _folderPath = "Assets/Resources/Eldorado";

        private float _tollerance = 0.082f;

        private int _model = 71087229;
        private int _count = 70;

        [MenuItem("Image360/Eldorado Downloader")]
        static void Init()
        {
            var window = (EldoradoDownloader)GetWindow(typeof(EldoradoDownloader));
            window.titleContent = new GUIContent("Downloader");
            window.Show();
        }

        #endregion
        #region Unity

        private bool isFoldoutBase = false;
        private bool isFoldoutModel = true;

        void OnGUI()
        {
            if (isFoldoutBase = EditorGUILayout.Foldout(isFoldoutBase, "Base"))
            {
                _baseUrl = EditorGUILayout.TextField("Base URL", _baseUrl);
                _folderPath = EditorGUILayout.TextField("Save path", _folderPath);
            }

            if (isFoldoutModel = EditorGUILayout.Foldout(isFoldoutModel, "Model"))
            {
                EditorGUILayout.LabelField("В ссылке вида https://www.eldorado.ru/cat/1613059/SAMSUNG/ '1613059'- это модель.");
                EditorGUILayout.LabelField("Count - количество скачиваемых изображений.");
                EditorGUILayout.LabelField("Tollerance - качество обработки изображений.");

                EditorGUILayout.Space();


                _model = EditorGUILayout.IntField("Model", _model);
                _count = EditorGUILayout.IntField("Count", _count);

                _tollerance = EditorGUILayout.FloatField("Tollerance", _tollerance);
            }


            if (GUILayout.Button("Download"))
            {
                var e = DownloadImages();
                while (e.MoveNext()) ;
            }
        }

        #endregion
        #region Download


        private IEnumerator DownloadImages()
        {
            for (int i = 1; i <= _count; i++)
            {
                var url = GetUrl(i);

                using (WWW www = new WWW(url))
                {
                    yield return www;

                    while (!www.isDone)
                        yield return null;

                    if (!Directory.Exists(_folderPath + "/" + _model))
                        Directory.CreateDirectory(_folderPath + " / " + _model);

                    File.WriteAllBytes(_folderPath + "/" + _model + "/" + i + ".png", GetReadyTexture(www.texture).EncodeToPNG());
                }
            }

            AssetDatabase.Refresh();
        }



        private string GetName(int idx)
        {
            string str;

            if (idx > 9 && idx < 100)
                str = "0" + idx;
            else if (idx < 10 && idx > 0)
                str = "00" + idx;
            else
                str = idx.ToString();

            return str;
        }

        private Texture2D GetReadyTexture(Texture2D texture)
        {
            var newTexture = ImageUtility.GetNewTexture(texture);

            ImageUtility.FloodFill(texture, newTexture, Color.white, _tollerance, 0, 0);

            newTexture.Apply();

            return newTexture;
        }




        private string GetUrl(int idx)
        {
            return _baseUrl + " / " + _model + "/" + GetName(idx) + ".jpg";
        }

        #endregion
    }
}
