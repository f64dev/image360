﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test
{
    public class BackButton : MonoBehaviour
    {
        #region Unity

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Selector.Current.OnClickBackButton();
        }

        #endregion
    }
}
