﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Test
{
    public class Selector : MonoBehaviour
    {
        #region Unity Editor

        [SerializeField]
        private string _scene2DName;

        [SerializeField]
        private string _scene3DName;

        [SerializeField]
        private string _sceneARName;

        [SerializeField]
        private string _sceneSelectorName;

        #endregion
        #region Unity

        public static Selector Current { get; private set; }

        private void Awake()
        {
            if (Current == null)
            {
                Current = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        #endregion
        #region UI Events

        public void OnClick2DButton()
        {
            SceneManager.LoadScene(_scene2DName, LoadSceneMode.Single);
        }

        public void OnClick3DButton()
        {
            SceneManager.LoadScene(_scene3DName, LoadSceneMode.Single);
        }

        public void OnClickARButton()
        {
            SceneManager.LoadScene(_sceneARName, LoadSceneMode.Single);
        }

        public void OnClickBackButton()
        {
            SceneManager.LoadScene(_sceneSelectorName, LoadSceneMode.Single);
        }

        #endregion

    }
}


