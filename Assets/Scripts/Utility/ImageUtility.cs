﻿using UnityEngine;
using System.Collections.Generic;



namespace Image360.Utility
{
    public static class ImageUtility
    {

        public struct Point
        {

            public int x;
            public int y;

            public Point(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        public static Texture2D GetNewTexture(Texture2D texture)
        {
            var newTexture = new Texture2D(texture.width, texture.height);
           newTexture.SetPixels32(texture.GetPixels32());

            return newTexture;
        }



        public static void FloodFill(Texture2D readTexture, Texture2D writeTexture, Color sourceColor, float tollerance, int x, int y)
        {
            var targetColor = Color.clear;

            var q = new Queue<Point>(readTexture.width * readTexture.height);
            q.Enqueue(new Point(x, y));
            int iterations = 0;

            var width = readTexture.width;
            var height = readTexture.height;
            while (q.Count > 0)
            {
                var point = q.Dequeue();
                var x1 = point.x;
                var y1 = point.y;
                if (q.Count > width * height)
                {
                    throw new System.Exception("The algorithm is probably looping. Queue size: " + q.Count);
                }

                if (writeTexture.GetPixel(x1, y1) == targetColor)
                {
                    continue;
                }

                writeTexture.SetPixel(x1, y1, targetColor);


                var newPoint = new Point(x1 + 1, y1);
                if (CheckValidity(readTexture, readTexture.width, readTexture.height, newPoint, sourceColor, tollerance))
                    q.Enqueue(newPoint);

                newPoint = new Point(x1 - 1, y1);
                if (CheckValidity(readTexture, readTexture.width, readTexture.height, newPoint, sourceColor, tollerance))
                    q.Enqueue(newPoint);

                newPoint = new Point(x1, y1 + 1);
                if (CheckValidity(readTexture, readTexture.width, readTexture.height, newPoint, sourceColor, tollerance))
                    q.Enqueue(newPoint);

                newPoint = new Point(x1, y1 - 1);
                if (CheckValidity(readTexture, readTexture.width, readTexture.height, newPoint, sourceColor, tollerance))
                    q.Enqueue(newPoint);

                iterations++;
            }
        }

        static bool CheckValidity(Texture2D texture, int width, int height, Point p, Color sourceColor, float tollerance)
        {
            if (p.x < 0 || p.x >= width)
            {
                return false;
            }
            if (p.y < 0 || p.y >= height)
            {
                return false;
            }

            var color = texture.GetPixel(p.x, p.y);

            var distance = Mathf.Abs(color.r - sourceColor.r) + Mathf.Abs(color.g - sourceColor.g) + Mathf.Abs(color.b - sourceColor.b);
            return distance <= tollerance;
        }
    } 
}